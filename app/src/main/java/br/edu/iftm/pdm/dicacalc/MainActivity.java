package br.edu.iftm.pdm.dicacalc;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String DEC_REGEX = "^\\d+|\\d*\\.\\d+|\\d+\\.\\d*$";
    private boolean resultMode;
    private float resultado;
    private boolean div = false;
    private boolean soma = false;
    private boolean sub = false;
    private boolean mult = false;
    private boolean result = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.resultMode = true;
    }

    public void onClickClean(View view) {
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        txtDisplay.setText("0");
        txtHistory.setText("");
        result = false;
        div = false;
        soma = false;
        sub = false;
        mult = false;
    }

    public void onClickDiv(View view) {
        div = true;
        soma = false;
        sub = false;
        mult = false;
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        if(result == true)
            resultado = resultado / new Float(txtDisplay.getText().toString());
        else
            resultado = new Float(txtDisplay.getText().toString());
        if (resultado % 1 == 0)
            txtHistory.setText((int) this.resultado + "÷");
        else
            txtHistory.setText(this.resultado + "÷");
        txtDisplay.setText("0");
    }

    public void onClickSoma(View view) {
        div = false;
        soma = true;
        sub = false;
        mult = false;
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        if(result == true)
            resultado = resultado + new Float(txtDisplay.getText().toString());
        else
            resultado = new Float(txtDisplay.getText().toString());
        if (resultado % 1 == 0)
            txtHistory.setText((int) this.resultado + "+");
        else
            txtHistory.setText(this.resultado + "+");
        txtDisplay.setText("0");
    }

    public void onClickSub(View view) {
        div = false;
        soma = false;
        sub = true;
        mult = false;
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        if(result == true)
            resultado = resultado - new Float(txtDisplay.getText().toString());
        else
            resultado = new Float(txtDisplay.getText().toString());
        if(resultado % 1 == 0)
            txtHistory.setText((int) this.resultado + "-");
        else
            txtHistory.setText(this.resultado + "-");
        txtDisplay.setText("0");
    }

    public void onClickMult(View view) {
        div = false;
        soma = false;
        sub = false;
        mult = true;
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        if(result == true)
            resultado = resultado * new Float(txtDisplay.getText().toString());
        else
            resultado = new Float(txtDisplay.getText().toString());
        if(resultado % 1 == 0)
            txtHistory.setText((int) this.resultado + "x");
        else
            txtHistory.setText(this.resultado + "x");
        txtDisplay.setText("0");
    }

    public void onClickResultado(View view) {
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        TextView txtHistory = findViewById(R.id.txtHistory);
        if(txtDisplay.getText().toString() != "0") {
            if (div) {
                resultado = resultado / (new Float(txtDisplay.getText().toString()));
                txtHistory.setText(Float.toString(this.resultado));
            } else if (soma) {
                resultado = resultado + (new Float(txtDisplay.getText().toString()));
                txtHistory.setText(Float.toString(this.resultado));
            } else if (sub) {
                resultado = resultado - (new Float(txtDisplay.getText().toString()));
                txtHistory.setText(Float.toString(this.resultado));
            } else {
                resultado = resultado * (new Float(txtDisplay.getText().toString()));
                txtHistory.setText(Float.toString(this.resultado));
            }
            result = true;
        }
        else
            return;
    }

    public void onClickDigit(View view) {
        Button button = (Button) view;
        TextView txtDisplay = findViewById(R.id.txtDisplay);
        if(this.resultMode) {
            if(button.getText().toString().equals("."))
                txtDisplay.setText("0.");
            else
                txtDisplay.setText(button.getText().toString());
            this.resultMode = false;
        }
        else {
            String v = txtDisplay.getText().toString();
            v += button.getText().toString();
            if(v.matches(DEC_REGEX)) {
                txtDisplay.append(button.getText().toString());
            }
            else {
                Log.d("DEBUGCALCULADORA", "nao combinou");
            }
        }
    }
}